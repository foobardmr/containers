#! /bin/sh
#
# Check if an images parent image (as in FROM ...) has changed. 
# Requires the annotations `org.opencontainers.image.base.name` 
# and `org.opencontainers.image.base.digest` to be present in 
# the images manifest. These annotations are usually set auto-
# matically when building images with buildah in `oci` format, 
# but absent in `docker` format.
# 
# Usage: 
#   check_for_updates <IMAGE>
#
# Exit codes:
#   0 - No parent image has changed
#   1 - At least one parent image has changed
#   2 - An error occurred
#

# Always exit with code 2 on error or abnormal termination
trap 'exit 2' ERR INT TERM
set -eo pipefail

for digest in `
    skopeo inspect --raw "docker://${1}" \
    | jq -r '.manifests[].digest'`
do
    image="${1%:*}@${digest}"
    image_manifest="$(skopeo inspect --raw "docker://${image}")"
    image_base_name="$(echo "${image_manifest}" | jq -r '.annotations.["org.opencontainers.image.base.name"]')"
    image_base_digest="$(echo "${image_manifest}" | jq -r '.annotations.["org.opencontainers.image.base.digest"]')"

    if ! skopeo inspect --raw "docker://${image_base_name}" \
       | jq -e --arg digest "${image_base_digest}" '.manifests[] | select(.digest==$digest)' > /dev/null
    then
        echo "[⚠] ${image} base image changed"
        exit 1
    else
        echo "[✓] ${image} is up to date"
    fi
done
